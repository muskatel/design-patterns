using System.Collections;

namespace IEnumerable;

public class BookEnumerator : IEnumerator
    {
        public Book[] _books;

        // Enumerators are positioned before the first element
        // until the first MoveNext() call.
        int position = -1;

        public BookEnumerator(Book[] list)
        {
            _books = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < _books.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Book Current
        {
            get
            {
                try
                {
                    return _books[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }