namespace IEnumerable;

using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json; //Install from Nuget: Newtonsoft.Json 13.0.03

public class BookReader
{
    public static List<Book> LoadBooksFromJson(string filePath)
    {
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("The specified file was not found.", filePath);
        }

        var jsonData = File.ReadAllText(filePath);
        return JsonConvert.DeserializeObject<List<Book>>(jsonData);
    }
}
