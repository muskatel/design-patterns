﻿using IEnumerable;

List<Book> tempBooks = BookReader.LoadBooksFromJson("book.json");

BookCollection books = new BookCollection(tempBooks.ToArray());

Console.WriteLine("Demo App: Iterator Pattern");

Console.WriteLine("--- Books: ---");

foreach(Book book in books)
{
    Console.WriteLine(book.ToString());
}


