using System.Collections;

namespace IEnumerable;

public class BookCollection:System.Collections.IEnumerable
{
    private Book[] _books;
    
    public BookCollection(Book[] bookArray)
    {
        _books = new Book[bookArray.Length];

        for (int i = 0; i < bookArray.Length; i++)
        {
            _books[i] = bookArray[i];
        }
    }
    
    IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public BookEnumerator GetEnumerator()
    {
        return new BookEnumerator(_books);
    }
}