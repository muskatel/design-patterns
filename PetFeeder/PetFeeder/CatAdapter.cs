namespace PetFeeder;

public class CatAdapter:IFeedable
{
    private Cat _cat;

    public CatAdapter(Cat cat)
    {
        _cat = cat;
    }
    public void Feed()
    {
        _cat.FeedTheCat();
    }
}