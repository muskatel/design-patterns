namespace PetFeeder;

//"new" interface
public interface IFeedable
{
    public void Feed();
}