namespace PetFeeder;

public class FishtankAdapter:IFeedable
{
    private FishTank _fishTank;

    public FishtankAdapter(FishTank fishTank)
    {
        _fishTank = fishTank;
    }
    public void Feed()
    {
        _fishTank.DropInSomeFlakes();
    }
}