namespace PetFeeder;

public class ModernCat:IFeedable
{
    private string name;

    public ModernCat(string name)
    {
        this.name = name;
    }
    public void Feed()
    {
        Console.WriteLine($"Fed {name} some food.");
    }
}