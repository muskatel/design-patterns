﻿// See https://aka.ms/new-console-template for more information

using PetFeeder;

// "Legacy" Methods
// Cat MrFlufflesworth = new Cat("MrFlufflesworth");
// MrFlufflesworth.FeedTheCat(); // Specific to the class, not generalized or compatible with the client
// FishTank algaeFarm = new FishTank();
// algaeFarm.DropInSomeFlakes();
 // Cat Steve = new Cat("Steve");

IFeedable egil = new CatAdapter(new Cat("Egil"));
egil.Feed();


List<IFeedable> thingsIneedtoFeed = new List<IFeedable>();

thingsIneedtoFeed.Add(new CatAdapter(new Cat("MrFlufflesworth")));
thingsIneedtoFeed.Add(new FishtankAdapter(new FishTank()));
thingsIneedtoFeed.Add(new CatAdapter(new Cat("Steve")));
thingsIneedtoFeed.Add(new ModernCat("Floof"));

foreach (IFeedable thing in thingsIneedtoFeed)
{
    thing.Feed();
}


// Save as txt
// save it into json
// save a picture





