namespace PetFeeder;
//Legacy code
public class Cat
{
    private string name;

    public Cat(string name)
    {
        this.name = name;
    }

    public void FeedTheCat()
    {
        Console.WriteLine($"Fed {name} some food.");
    }
}