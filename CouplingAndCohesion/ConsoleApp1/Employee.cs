namespace ConsoleApp1;

public class Employee
{
    private Person _person;
    private string _position;

    
    // could instead require a class of type IHasName
    
    public Employee(Person person, string position)
    {
        _person = person;
        _position = position;
    }

    public override string ToString()
    {
        return $"{_person.Name} with positon {_position}";
    }
}