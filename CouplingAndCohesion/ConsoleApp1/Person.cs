namespace ConsoleApp1;

// could implement a new inteface of type IHasName

public class Person
{
    private string _firstName;
    private string _lastName;
    private string _email;

    public Person(string first_name, string last_name, string email)
    {
        _firstName = first_name;
        _lastName = last_name;
        _email = email;
    }

    public string Name
    {
        get => _firstName + " " + _lastName;
    }

    public string Email
    {
        get => _email;
        set => _email = value;
    }

    public override string ToString()
    {
        return $"{Name}: {_email}";
    }
}