namespace CouplingAndCohesion;

public class BookShelf
{
    private List<Book> books = new List<Book>();

    public BookShelf()
    {
        books.Add(new Book("The Bible"));
        books.Add(new Book("Hitchhikers Guide to the Galaxy"));
        books.Add(new Book("The Davinci Code"));
        books.Add(new Book("The Hobbit"));
        books.Add(new Book("Children of Time"));
    }
    
    //indexer
    public Book this[int index] 
    {
        get => books[index];
    }

    /// <summary>
    /// Gets a book based on it's index
    /// </summary>
    /// <param name="index">The index of the book</param>
    /// <returns>A book</returns>
    public Book gb(int i)
    {
        return books[i];
    }
    
    //Better practice, no need for a comment
    public Book GetBook(int index)
    {
        return books[index];
    }
    
    
}