namespace CouplingAndCohesion;

public class Book
{
    public string Name;

    public Book(string name)
    {
        Name = name;
    }

    public override string ToString()
    {
        return Name;
    }
}