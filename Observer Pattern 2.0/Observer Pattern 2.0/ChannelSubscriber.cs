namespace Observer_Pattern_2._0;

public class ChannelSubscriber:IObserver<Video>
{
    
    private IDisposable unsubscriber;
    public virtual void Subscribe(IObservable<Video> provider)
    {
        if (provider != null)
            unsubscriber = provider.Subscribe(this);
    }
    
    public virtual void Unsubscribe()
    {
        unsubscriber.Dispose();
    }
    
    public void OnCompleted()
    {
        Console.WriteLine("Completed...");
    }

    public void OnError(Exception error)
    {
        Console.WriteLine(error.Message);
    }

    public void OnNext(Video value)
    {
        Console.WriteLine(value.data);
    }
}