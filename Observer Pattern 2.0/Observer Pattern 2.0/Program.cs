﻿// See https://aka.ms/new-console-template for more information


using Observer_Pattern_2._0;

ChannelSubscriber subscriber = new ChannelSubscriber();

ChannelTracker channelTracker = new ChannelTracker();

subscriber.Subscribe(channelTracker);

Video video = new Video();
video.data = "Hello, World";

channelTracker.NotifySubscribers(video);

subscriber.Unsubscribe();
