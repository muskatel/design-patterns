using System.Threading.Channels;

namespace Observer_Pattern_2._0;


public class ChannelTracker:IObservable<Video>
{
    private List<IObserver<Video>> _observers;

    public ChannelTracker()
    {
        _observers = new List<IObserver<Video>>();
    }

    public void NotifySubscribers(Video video)
    {
        foreach (IObserver<Video> observer in _observers)
        {
            observer.OnNext(video);
        }
    }

    public IDisposable Subscribe(IObserver<Video> observer)
    {
        if (!_observers.Contains(observer))
        {
            _observers.Add(observer);
        }

        return new Unsubscriber(_observers, observer);
    }

    public class Unsubscriber : IDisposable
    {
        private List<IObserver<Video>> _observers;
        private IObserver<Video> _observer;

        public Unsubscriber(List<IObserver<Video>> observers, IObserver<Video> observer)
        {
            _observers = observers;
            _observer = observer;
        }
        public void Dispose()
        {
            if (_observer != null && _observers.Contains(_observer))
            {
                _observers.Remove(_observer);
            }
        }
    }
}