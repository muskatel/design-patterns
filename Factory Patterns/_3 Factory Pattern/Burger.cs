﻿namespace Factory_Patterns;

public abstract class Burger
{
    
    public abstract string GetName();
    public abstract string GetPrice();
    public abstract IEnumerable<string> GetIngredients();
    
}