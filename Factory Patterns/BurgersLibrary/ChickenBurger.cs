﻿namespace BurgersLibrary;
public class ChickenBurger:Burger
{
    public override void PrepareBun(string bun)
    {
        AddIngredient(bun);
    }

    public override void AddSauce()
    {
        AddIngredient("Mayonaise");
    }

    public override void AddPatty()
    {
        AddIngredient("Chicken Breast");
    }

    public override void AddToppings()
    {
        AddIngredient("Fresh Lettuce");
    }

    public override void WrapBurger()
    {
        Console.WriteLine($"{GetName()} burger is wrapped and ready!");
    }

    public ChickenBurger() : base("Chicken", 49.80f)
    {
    }
}