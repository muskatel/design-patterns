﻿namespace BurgersLibrary;

public abstract class Burger
{
    private string name;
    private float price;
    
    private List<string> ingredients = new List<string>();
    
    protected Burger(string name, float price)
    {
        this.name = name;
        this.price = price;
    }
    
    public abstract void PrepareBun(string bun);
    public abstract void AddSauce();
    public abstract void AddPatty();
    public abstract void AddToppings();
    public abstract void WrapBurger();

    public string GetName()
    {
        return name;
    }

    public float GetPrice()
    {
        return price;
    }

    public override string ToString()
    {
        return $"A {name} burger costs {price:F2}kr. ";
    }

    protected void AddIngredient(string ingredient)
    {
        ingredients.Add(ingredient);
    }

    public IEnumerable<string> GetIngredients()
    {
        return ingredients;
    }
    
}