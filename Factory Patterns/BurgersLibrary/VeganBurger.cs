﻿namespace BurgersLibrary;
public class VeganBurger:Burger
{
    public override void PrepareBun(string bun)
    {
        AddIngredient(bun);
    }

    public override void AddSauce()
    {
        AddIngredient("Ketchup");
    }

    public override void AddPatty()
    {
        AddIngredient("I-cant-believe-it's-not-meat-patty™");
    }

    public override void AddToppings()
    {
        AddIngredient("Fresh Lettuce");
    }

    public override void WrapBurger()
    {
        Console.WriteLine($"{GetName()} burger is wrapped and ready!");
    }

    public VeganBurger() : base("Vegan", 79.80f)
    {
    }
    
}