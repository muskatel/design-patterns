﻿namespace BurgersLibrary;
public class CheeseBurger:Burger
{
    //pretend this is a complex class that cant use a simple constructor


    public override void PrepareBun(string bun)
    {
        AddIngredient(bun);
    }

    public override void AddSauce()
    {
        AddIngredient("Ketchup");
    }

    public override void AddPatty()
    {
        AddIngredient("Beef Patty");
    }

    public void AddCheese()
    {
        AddIngredient("Delicious CHEESE");
    }

    public override void AddToppings()
    {
        AddIngredient("2 Gerkin Slices");
    }

    public override void WrapBurger()
    {
        Console.WriteLine($"{GetName()} burger is wrapped and ready!");
    }

    public CheeseBurger() : base("Cheese", 54.80f)
    {
    }
}

