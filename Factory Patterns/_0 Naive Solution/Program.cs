﻿
using BurgersLibrary;

Console.WriteLine("-------------------------------");
Console.WriteLine(" Welcome to burger World! Alpha");
Console.WriteLine("-------------------------------");

// Cheese burger 

CheeseBurger myBurger = new CheeseBurger();
myBurger.PrepareBun("Regular Bun");
myBurger.AddSauce();
myBurger.AddPatty();
myBurger.AddCheese();
myBurger.AddToppings();
myBurger.WrapBurger();

Console.WriteLine(myBurger.ToString());
Console.WriteLine("Ingredients:");
foreach (string item in myBurger.GetIngredients())
{
    Console.WriteLine($" - {item}");
}  