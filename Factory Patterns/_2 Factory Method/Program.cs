﻿using BurgersLibrary;
using Factory_Patterns;

Console.WriteLine("-----------------------------");
Console.WriteLine(" Welcome to burger World v2! ");
Console.WriteLine("-----------------------------");

CheeseBurgerMaker cheeseBurgerMaker = new CheeseBurgerMaker();
ChickenBurgerMaker chickenBurgerMaker = new ChickenBurgerMaker();
VeganBurgerMaker veganBurgerMaker = new VeganBurgerMaker();


string desiredBurger = "cheese";
//string desiredBurger = "chicken";
//string desiredBurger = "vegan";

Burger myBurger = null;
BurgerMaker currentMaker = null;

switch (desiredBurger)
{
    case "cheese":
        currentMaker = cheeseBurgerMaker;
        break;
    case "chicken":
        currentMaker = chickenBurgerMaker;
        break;
    case "vegan":
        currentMaker = veganBurgerMaker;
        break;
    default:
        Console.WriteLine("Unknown Burger");
        break;
}

myBurger = currentMaker.PrepareBurger();

Console.WriteLine(myBurger.ToString());
Console.WriteLine("Ingredients:");
foreach (string item in myBurger.GetIngredients())
{
    Console.WriteLine($" - {item}");
}  
