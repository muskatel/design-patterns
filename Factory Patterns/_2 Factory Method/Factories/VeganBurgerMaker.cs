﻿using BurgersLibrary;

namespace Factory_Patterns;

public class VeganBurgerMaker : BurgerMaker
{
    public override Burger PrepareBurger()
    {
        ChickenBurger newVeganBurger = new ChickenBurger();
        newVeganBurger.PrepareBun("Vegan Bun");
        newVeganBurger.AddSauce();
        newVeganBurger.AddPatty();
        newVeganBurger.AddToppings();
        newVeganBurger.WrapBurger();

        return newVeganBurger;
    }
}