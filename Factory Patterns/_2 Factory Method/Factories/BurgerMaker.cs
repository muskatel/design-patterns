﻿using BurgersLibrary;

namespace Factory_Patterns;

public abstract class BurgerMaker
{
    public abstract Burger PrepareBurger(); // This the abstract method which acts a factory for burgers
}