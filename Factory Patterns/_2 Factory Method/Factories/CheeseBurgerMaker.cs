﻿using BurgersLibrary;

namespace Factory_Patterns;

public class CheeseBurgerMaker:BurgerMaker
{
    public override Burger PrepareBurger()
    {
        CheeseBurger newCheeseBurger = new CheeseBurger();
        newCheeseBurger.PrepareBun("Regular Bun");
        newCheeseBurger.AddSauce();
        newCheeseBurger.AddPatty();
        newCheeseBurger.AddCheese(); //  no cast required
        newCheeseBurger.AddToppings();
        newCheeseBurger.WrapBurger();

        return newCheeseBurger;
    }
}