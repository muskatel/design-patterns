﻿using BurgersLibrary;

namespace Factory_Patterns;

public class ChickenBurgerMaker : BurgerMaker
{
    public override Burger PrepareBurger()
    {
        ChickenBurger newChickenBurger = new ChickenBurger();
        newChickenBurger.PrepareBun("Regular Bun");
        newChickenBurger.AddPatty();
        newChickenBurger.AddSauce();
        newChickenBurger.AddToppings();
        newChickenBurger.WrapBurger();

        return newChickenBurger;
    }
}