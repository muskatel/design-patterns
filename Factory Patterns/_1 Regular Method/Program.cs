﻿using BurgersLibrary;

Console.WriteLine("--------------------------");
Console.WriteLine(" Welcome to burger World! ");
Console.WriteLine("--------------------------");

// Cheese burger 
// Chicken burger
// Vegan burger

//string desiredBurger = "cheese";
//string desiredBurger = "chicken";
string desiredBurger = "vegan";

Burger myBurger = null;


// The current class represents the User Interface so it really should not have knowledge of the construction of burgers!
// Meaning, this switch statement is bad!
switch (desiredBurger)
{
    case "cheese": 
        myBurger = new CheeseBurger();
        myBurger.PrepareBun("Regular Bun");
        myBurger.AddSauce();
        myBurger.AddPatty();
        ((CheeseBurger)myBurger).AddCheese(); // We have to know to cast this particular burger to access the AddCheese() method
        myBurger.AddToppings();
        myBurger.WrapBurger();
        break;
    case "chicken":
        myBurger = new ChickenBurger();
        myBurger.PrepareBun("Regular Bun");
        myBurger.AddPatty();
        myBurger.AddSauce(); // A chicken burger takes sauce after patty
        myBurger.AddToppings();
        myBurger.WrapBurger();
        break;
    case "vegan":
        myBurger = new VeganBurger();
        myBurger.PrepareBun("Vegan Bun");
        myBurger.AddSauce();
        myBurger.AddPatty();
        myBurger.AddToppings();
        myBurger.WrapBurger();
        break;
    default:
        Console.WriteLine("Unknown Burger");
        break;
        
}

Console.WriteLine(myBurger.ToString());
Console.WriteLine("Ingredients:");
foreach (string item in myBurger.GetIngredients())
{
    Console.WriteLine($" - {item}");
}  

