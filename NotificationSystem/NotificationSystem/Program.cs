﻿// See https://aka.ms/new-console-template for more information


using NotificationSystem.Notification;
/*
INotification notification = new SimpleNotification("Eat your greens!");


Console.WriteLine("Simple Message:");
Console.Write(notification.GetMessage());
Console.Write("\n\n");

// Add a timestamp
notification = new TimestampDecorator(notification);


Console.Write(notification.GetMessage());
Console.Write("\n\n");

// Make is pop!
// notification = new ImportantDecorator(notification);


Console.Write(notification.GetMessage());
Console.Write("\n\n");

// Give it a title
notification = new TitleDecorator(notification, "A message from mom");

Console.Write(notification.GetMessage());
Console.Write("\n\n");
*/
// Another example

INotification slidingIntoTheirDMs = new SimpleNotification("Hey, you wanna help me decorate some patterns later?");
// slidingIntoTheirDMs = new ImportantDecorator(slidingIntoTheirDMs);
slidingIntoTheirDMs = new TimestampDecorator(slidingIntoTheirDMs);
//slidingIntoTheirDMs = new HighlightDecorator(slidingIntoTheirDMs);
// slidingIntoTheirDMs = new TitleDecorator(slidingIntoTheirDMs,"sup");
Console.Write(slidingIntoTheirDMs.GetMessage());
