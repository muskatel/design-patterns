namespace NotificationSystem.Notification;

public class HighlightDecorator:NotificationDecorator
{
    public HighlightDecorator(INotification baseNotification) : base(baseNotification)
    {
    }

    public override string GetMessage()
    {
        return $"--> {_baseNotification.GetMessage()} <--";
    }
}