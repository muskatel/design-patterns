namespace NotificationSystem.Notification;

public class ImportantDecorator:NotificationDecorator
{
    public ImportantDecorator(INotification baseNotification) : base(baseNotification)
    {
        // our base class handles the construction 
    }

    public override string GetMessage()
    {
        return $"IMPORTANT: \n{_baseNotification.GetMessage()}";
        
    }
}