namespace NotificationSystem.Notification;

public abstract class NotificationDecorator:INotification
{
    protected INotification _baseNotification;

    public NotificationDecorator(INotification baseNotification)
    {
        _baseNotification = baseNotification;
    }

    public abstract string GetMessage();
}
