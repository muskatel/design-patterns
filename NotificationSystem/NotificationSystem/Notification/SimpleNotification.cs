namespace NotificationSystem.Notification;

public class SimpleNotification:INotification
{
    private string _message;

    public SimpleNotification(string message)
    {
        // A simple notification is constructed from a string
        _message = message;
    }
    public string GetMessage()
    {
        // A simple notification returns the contained string
        return _message;
    }
}