
namespace NotificationSystem.Notification;

public interface INotification
{
    string GetMessage();
}