namespace NotificationSystem.Notification;

public class EmojiDecorator : NotificationDecorator
{
    public EmojiDecorator(INotification baseNotification) : base(baseNotification)
    {
    }

    public override string GetMessage()
    {
        return $"\ud83d\ude80 {_baseNotification.GetMessage()} \ud83d\ude80";
    }
}