namespace NotificationSystem.Notification;

public class TitleDecorator:NotificationDecorator
{
    private string _title;
    public TitleDecorator(INotification baseNotification, string title) : base(baseNotification)
    {
        // Here we must allocate the title
        _title = title;
    }

    public override string GetMessage()
    {
        return $"Title: {_title}\n{_baseNotification.GetMessage()}";    
    }
}