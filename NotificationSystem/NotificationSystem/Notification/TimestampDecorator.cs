namespace NotificationSystem.Notification;

class TimestampDecorator:NotificationDecorator
{
    public TimestampDecorator(INotification baseNotification) : base(baseNotification)
    {
        // our base class handles the construction 
    }

    public override string GetMessage()
    {
        return $"[{DateTime.Now}] {_baseNotification.GetMessage()}";
    }
}