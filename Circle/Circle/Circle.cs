namespace Circle;

public class Circle
{
    private float positionX;
    private float positionY;
    private float radius;
    
    public float PositionX
    {
        get => positionX;
        set => positionX = value;
    }

    public float PositionY
    {
        get => positionY;
        set => positionY = value;
    }

    public float Radius
    {
        get => radius;
        set => radius = value;
    }
    
    public Circle(float x, float y, float radius)
    {
        this.positionX = x;
        this.positionY = y;
        this.radius = radius;
    }

    public float GetArea()
    {
        return (float)Math.PI * radius * radius;
    }
    
    public float GetCircumference()
    {
        return (float)Math.PI * 2 * radius;
    }
}