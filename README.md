# Design Patterns

> Personal version of my notes on Desing Patterns for use in class.

### Software Best Practices

- **Modularity**:
		The software system should be divided into modules, where each module performs a specific function. This makes it easier to develop, test and maintain the system.
- **Scalability**:
		The software system should be designed to handle changes in requirements, data volume and user load. This means that the system should be able to be scaled up or down as needed.
- **Flexibility**:
		The software system should be flexible and adaptable to changes. It should be designed in a way that allows modifications and extensions without requiring significant changes to existing code.
- **Maintainability**:
		The software system should be easy to maintain and update. This means that the code should be well organized, easy to understand and easy to change.
- **Testability**:
		The software system should be designed in a way that makes it easy to test. This means that the code should be written in a way that makes it easy to test each module individually.
- **Reusability**:
		The software system should be designed in a way that allows code reuse. This means that modules should be designed to be used in different contexts and applications.
- **Performance**:
		The software system should be designed to be efficient and perform well. This means that it should be able to handle a large number of users and transactions without experiencing significant delays.

### Creational Patterns

Creational patterns deal with object creation mechanisms, trying to create objects in a manner suitable to the situation.

- **Singleton**:
        Ensures a class has only one instance and provides a global point of access.
- **Factory Method**:
        Allows a class to delegate the responsibility of instantiating its objects to subclasses.
- **Abstract Factory**:
        Provides an interface for creating families of related or dependent objects without specifying their concrete classes.
- **Builder**:
        Separates the construction of a complex object from its representation.
- **Prototype**:
        Creates a fully initialized instance that can be cloned or copied to produce a new instance.

### Structural Patterns

Structural patterns concern class and object composition. They use inheritance to compose interfaces or implementations.

- **Adapter**:
        Allows objects with incompatible interfaces to collaborate.
- **Bridge**:
        Decouples an abstraction from its implementation so the two can vary independently.
- **Composite**:
        Allows you to compose objects into tree structures to represent part-whole hierarchies.
- **Decorator**:
        Adds responsibilities to objects dynamically.
- **Facade**:
        Provides a unified interface to a set of interfaces in a subsystem.
- **Flyweight**:
        Uses sharing to support a large number of fine-grained objects efficiently.
- **Proxy**:
        Acts as a surrogate or placeholder for another object.

### Behavioral Patterns

Behavioral patterns define ways of communication between objects.

- **Command**:
        Turns a request into a stand-alone object that contains information about the request.
- **Chain of Responsibility**:
        Lets more than one object handle a request by chaining receiving objects together.
- **Interpreter**:
        Provides a way to evaluate language grammar or expressions for particular languages.
- **Iterator**:
        Provides a way to access the elements of an aggregate object without exposing its underlying representation.
- **Mediator**:
        Reduces direct communication between objects by making objects communicate via a mediator object.
- **Memento**:
        Lets you save and restore the previous state of an object.
- **Observer**:
        Lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they're observing.
- **State**:
        Allows an object to change its behavior when its internal state changes.
- **Strategy**:
        Defines a family of algorithms, puts each one into a separate class, and makes them interchangeable.
- **Template Method**:
        Defines the skeleton of an algorithm in a base class but lets subclasses override specific steps without changing the algorithm's structure.
- **Visitor**:
        Lets you add further operations to objects without having to modify them.


### Principles

- **SOLID**:
- **GRASP**:
- **DRY**:
- **KISS**:
- **YAGNI**:


## Maintainers

Craig Marais @muskatel

## License

MIT License, see license file.

---

Copyright 2023, Craig Marais (@muskatel)
