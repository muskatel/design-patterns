namespace PizzaMaker3000.Pizzas.Toppings;

public abstract class PizzaTopping:IPizza
{
    protected IPizza _pizza;

    protected PizzaTopping(IPizza pizza)
    {
        _pizza = pizza;
    }

    public abstract float Price();

    public abstract string Toppings();
}