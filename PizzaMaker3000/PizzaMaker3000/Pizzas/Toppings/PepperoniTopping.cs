namespace PizzaMaker3000.Pizzas.Toppings;

public class AddPepperoni:PizzaTopping
{
    public AddPepperoni(IPizza pizza) : base(pizza)
    {
    }

    public override float Price()
    {
        return _pizza.Price() + 12.50f;
    }

    public override string Toppings()
    {
        return _pizza.Toppings() + "\n + Pepperoni";
    }
}

public class RemovePepperoni:PizzaTopping
{
    public RemovePepperoni(IPizza pizza) : base(pizza)
    {
    }

    public override float Price()
    {
        return _pizza.Price() - 12.50f;
    }

    public override string Toppings()
    {
        return _pizza.Toppings() + "\n - Pepperoni";
    }
}