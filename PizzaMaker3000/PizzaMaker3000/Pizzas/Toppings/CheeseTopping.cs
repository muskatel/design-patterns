namespace PizzaMaker3000.Pizzas.Toppings;

public class AddCheese:PizzaTopping
{
    public AddCheese(IPizza pizza) : base(pizza)
    {
    }

    public override float Price()
    {
        return _pizza.Price() + 9.87f;
    }

    public override string Toppings()
    {
        return _pizza.Toppings() + "\n + Cheese";
    }
}

public class RemoveCheese:PizzaTopping
{
    public RemoveCheese(IPizza pizza) : base(pizza)
    {
    }

    public override float Price()
    {
        
        return _pizza.Price() - 9.87f;
    }

    public override string Toppings()
    {
        return _pizza.Toppings() + "\n - Cheese";
    }
}