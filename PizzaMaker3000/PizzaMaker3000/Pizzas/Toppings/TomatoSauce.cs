namespace PizzaMaker3000.Pizzas.Toppings;

public class AddTomatoSauce:PizzaTopping
{
    public AddTomatoSauce(IPizza pizza) : base(pizza)
    {
    }

    public override string Toppings()
    {
        return _pizza.Toppings() + "\n + Tomato Sauce";
    }

    public override float Price()
    {
        return _pizza.Price() + 5.0f;
    }
}

public class RemoveTomatoSauce:PizzaTopping
{
    public RemoveTomatoSauce(IPizza pizza) : base(pizza)
    {
    }

    public override float Price()
    {
        
        return _pizza.Price() - 5.0f;
    }

    public override string Toppings()
    {
        return _pizza.Toppings() + "\n - Tomato Sauce";
    }
}