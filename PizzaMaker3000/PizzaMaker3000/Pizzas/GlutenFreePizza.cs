namespace PizzaMaker3000.Pizzas;

public class GlutenFreePizza:IPizza
{
    
    public float Price()
    {
        return 49.90f;
    }

    public string Toppings()
    {
        return "--- Gluten Free Pizza ---";
    }
}