namespace PizzaMaker3000.Pizzas;

public class NormalPizza:IPizza
{
    public float Price()
    {
        return 29.90f;
    }

    public string Toppings()
    {
        return "--- Normal Pizza ---";
    }
}