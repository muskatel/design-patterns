namespace PizzaMaker3000.Pizzas;

public interface IPizza
{
    public float Price();
    public string Toppings();
}