namespace PizzaMaker3000.Pizzas;

public class ThickBasePizza:IPizza
{
    public float Price()
    {
        return 39.90f;
    }

    public string Toppings()
    {
        return "--- Thick Base Pizza ---";
    }
}