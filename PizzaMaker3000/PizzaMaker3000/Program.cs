﻿// See https://aka.ms/new-console-template for more information

using PizzaMaker3000.Pizzas;
using PizzaMaker3000.Pizzas.Toppings;

IPizza myPizza = new ThickBasePizza();
myPizza = new AddTomatoSauce(myPizza);
myPizza = new AddCheese(myPizza);
myPizza = new AddCheese(myPizza);
myPizza = new AddPepperoni(myPizza);
myPizza = new RemovePepperoni(myPizza);

Console.Write(myPizza.Toppings());

Console.WriteLine($"\n\nCost of pizza is {myPizza.Price().ToString("F2")} kr");