﻿using Newtonsoft.Json;

namespace Singleton_Pattern;

public class BookDatabase
{
    // Lazy instantiation, but later we will need to retrieve the value
    private static Lazy<BookDatabase> _instance = new Lazy<BookDatabase>(() => new BookDatabase());
    
    // A store of the books.
    private List<Book> _books;

    public static BookDatabase GetInstance
    {
        get
        {
            return _instance.Value;
        }
    }
    
    //Same as above, just `simpler` syntax (expression-bodied property)
    //
    //      An expression-bodied method consists of a single expression that
    //      returns a value whose type matches the method's return type, or,
    //      for methods that return void, that performs some operation. 
    //
    public static BookDatabase Instance => _instance.Value;
    
    private BookDatabase()
    {
        _books = LoadBooksFromJson("books.json");
    }
    
    private List<Book> LoadBooksFromJson(String filePath)
    {
        Console.WriteLine("Reading books from \"" + filePath + "\" now...");
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("The specified file was not found.", filePath);
        }
        
        var jsonData = File.ReadAllText(filePath);
        List<Book>? loadedBooks = JsonConvert.DeserializeObject<List<Book>>(jsonData);
        Console.WriteLine("--- Loaded " + loadedBooks.Count + " books from disk. ---");
        return loadedBooks;
    }

    public IEnumerable<Book> SearchBooks(String search)
    {
        foreach (Book b in _books)
        {
            if (b.Title.ToUpper().Contains(search.ToUpper()) || b.Author.ToUpper().Contains(search.ToUpper()))
            {
                yield return b;
            }
            
        }
    }
}