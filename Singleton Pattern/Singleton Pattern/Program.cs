﻿namespace Singleton_Pattern;

public static class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("\n---------  First Search: Tolkien   ---------\n");
        FindBooksWith("Tolkien");
        
        Console.WriteLine("\n---------  Second Search: R.R.   ---------\n");
        FindBooksWith("R.R.");
        
        Console.WriteLine("\n---------  Third Search: The   ---------\n");
        FindBooksWith("The");
    }

    static void FindBooksWith(String search)
    {
        Console.WriteLine($"Searching for \"{search}\" using the BookDatabase");

        foreach (Book book in BookDatabase.Instance.SearchBooks(search))
        {
            Console.WriteLine($"Found {book}");
        }
    }


}