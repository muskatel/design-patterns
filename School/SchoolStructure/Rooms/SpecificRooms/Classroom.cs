namespace School.SchoolStructure.Rooms.SpecificRooms;

public class Classroom:Room, IDoor
{
    public void Door()
    {
        Console.WriteLine($"used the door in {Name}");
    }
}