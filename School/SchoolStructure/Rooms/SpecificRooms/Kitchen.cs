namespace School.SchoolStructure.Rooms.SpecificRooms;

public abstract class Kitchen: Room
{
    public abstract void Sink();
    
}