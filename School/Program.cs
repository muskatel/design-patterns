﻿// See https://aka.ms/new-console-template for more information

using School.SchoolStructure.Rooms;
using School.SchoolStructure.Rooms.SpecificRooms;

List<Room> schoolBuildingRooms = new List<Room>();

Console.WriteLine("Hello, World!");

Classroom classroom = new Classroom();
classroom.Name = "Svenner";
classroom.Door();

schoolBuildingRooms.Add(classroom);

Kitchen studentKitchen = new CommonKitchen();
studentKitchen.Name = "The open areas";

schoolBuildingRooms.Add(studentKitchen);

foreach (Room room in schoolBuildingRooms)
{
    Console.WriteLine(room.Name);
}