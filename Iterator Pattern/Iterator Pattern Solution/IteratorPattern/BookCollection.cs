namespace IteratorPattern;

public class BookCollection : IBookCollection
{
    private Book[] _books;
    
    List<Book> _tempBooks = BookReader.LoadBooksFromJson("book.json");

    public BookCollection()
    {
        _books = new Book[_tempBooks.Count];
        for (int i = 0; i < _tempBooks.Count; i++)
        {
            _books[i] = _tempBooks[i];
        }
    }

    public IBookIterator CreateIterator()
    {
        return new BookIterator(this);
    }

    public int Length => _books.Length;

    public Book this[int index] => _books[index];
}