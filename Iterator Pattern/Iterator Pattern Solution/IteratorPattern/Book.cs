namespace IteratorPattern;

/// <summary>
/// A simple book object
/// </summary>
public class Book
{
    public Book(string Title, string Author,string Genre,int Year_Published,string ISBN)
    {
        this.Title = Title;
        this.Author = Author;
        this.Genre = Genre;
        this.Year_Published = Year_Published;
        this.ISBN = ISBN;
    }
    
    public string Title { get; set; }
    public string Author { get; set; }
    public string Genre { get; set; }
    public int Year_Published { get; set; }
    public string ISBN { get; set; }
    
    public override string ToString()
    {
        return $"{Title} by {Author}";
    }
}
