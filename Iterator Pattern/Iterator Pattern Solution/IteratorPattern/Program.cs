﻿using IteratorPattern;


// Create the collection
BookCollection bookCollection = new BookCollection();

// create the iterator
IBookIterator iterator = bookCollection.CreateIterator();

// Code below is independent of Book, only uses Iterator
// public void DisplayItems(IBookIterator iterator)
{
    Console.WriteLine("Demo App: Iterator Pattern");

    Console.WriteLine("--- Books: ---");

// Go the the start of the collection
    iterator.First();

// While not at the end
/*
    while (!iterator.IsDone())
    {
        // Print the current book
        Console.WriteLine(iterator.CurrentItem().ToString());

        // got the next book
        iterator.Next();
    }
    */

    Console.WriteLine(iterator.First());   //First
    Console.WriteLine(iterator.Next());    //Second
    Console.WriteLine(iterator.Next());    //Third
    
    Console.WriteLine(iterator.CurrentItem());
}

