namespace IteratorPattern;

/// <summary>
/// A concrete implementation of the Iterator
/// </summary>
public class BookIterator : IBookIterator
{
    private BookCollection _collection;
    private int _currentIndex = 0;

    public BookIterator(BookCollection collection)
    {
        _collection = collection;
    }

    public Book First()
    {
        return _collection[0];
    }

    public Book Next()
    {
        _currentIndex++;
        if (IsDone())
            return null;
        return _collection[_currentIndex];
    }

    public bool IsDone()
    {
        return _currentIndex >= _collection.Length;
    }

    public Book CurrentItem()
    {
        return _collection[_currentIndex];
    }
}