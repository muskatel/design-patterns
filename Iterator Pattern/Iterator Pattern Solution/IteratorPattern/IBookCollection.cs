namespace IteratorPattern;

public interface IBookCollection
{
    IBookIterator CreateIterator();
}