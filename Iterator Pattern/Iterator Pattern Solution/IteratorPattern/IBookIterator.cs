namespace IteratorPattern;


/// <summary>
/// This is a basic example of an iterator.
/// It provides a standardized way fo moving through an aggregated collection of books.
/// The underlying structure of the collection is not relevant at this stage.
/// </summary>
public interface IBookIterator
{
    Book First();
    Book Next();
    bool IsDone();
    Book CurrentItem();
}