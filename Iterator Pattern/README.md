# Iterator Pattern

Consider a collection of books.

**books.json**:
```json
[
  {
    "title": "The Lord of the Rings",
    "author": "J.R.R. Tolkien",
    "year_published": 1954,
    "isbn": "9780261103252"
  },
  {
    "title": "Harry Potter and the Philosopher's Stone",
    "author": "J.K. Rowling",
    "year_published": 1997,
    "isbn": "9780747532699"
  },
  {
    "title": "A Game of Thrones",
    "author": "George R.R. Martin",
    "year_published": 1996,
    "isbn": "9780553103540"
  },
  {
    "title": "The Lion, the Witch and the Wardrobe",
    "author": "C.S. Lewis",
    "year_published": 1950,
    "isbn": "9780064471046"
  },
  {
    "title": "The Eye of the World",
    "author": "Robert Jordan",
    "year_published": 1990,
    "isbn": "9780312850098"
  },
  {
    "title": "The Hobbit",
    "author": "J.R.R. Tolkien",
    "year_published": 1937,
    "isbn": "9780547928227"
  },
  {
    "title": "The Last Wish",
    "author": "Andrzej Sapkowski",
    "year_published": 1993,
    "isbn": "9780316438964"
  },
  {
    "title": "The Gunslinger",
    "author": "Stephen King",
    "year_published": 1982,
    "isbn": "9780452284692"
  },
  {
    "title": "Northern Lights (The Golden Compass)",
    "author": "Philip Pullman",
    "year_published": 1995,
    "isbn": "9780590660549"
  },
  {
    "title": "The Colour of Magic",
    "author": "Terry Pratchett",
    "year_published": 1983,
    "isbn": "9780861403240"
  },
  {
    "title": "Dune",
    "author": "Frank Herbert",
    "year_published": 1965,
    "isbn": "9780441013593"
  },
  {
    "title": "Foundation",
    "author": "Isaac Asimov",
    "year_published": 1951,
    "isbn": "9780553293357"
  },
  {
    "title": "1984",
    "author": "George Orwell",
    "year_published": 1949,
    "isbn": "9780452284234"
  },
  {
    "title": "Ender's Game",
    "author": "Orson Scott Card",
    "year_published": 1985,
    "isbn": "9780812550702"
  },
  {
    "title": "Brave New World",
    "author": "Aldous Huxley",
    "year_published": 1932,
    "isbn": "9780060850524"
  },
  {
    "title": "Starship Troopers",
    "author": "Robert A. Heinlein",
    "year_published": 1959,
    "isbn": "9780441783588"
  },
  {
    "title": "The Left Hand of Darkness",
    "author": "Ursula K. Le Guin",
    "year_published": 1969,
    "isbn": "9780441478125"
  },
  {
    "title": "Neuromancer",
    "author": "William Gibson",
    "year_published": 1984,
    "isbn": "9780441569595"
  },
  {
    "title": "Red Mars",
    "author": "Kim Stanley Robinson",
    "year_published": 1992,
    "isbn": "9780553560732"
  },
  {
    "title": "The War of the Worlds",
    "author": "H.G. Wells",
    "year_published": 1898,
    "isbn": "9780486295060"
  }
]
```

# The Base Program

Cosnider the following code:

**Program.cs**:
```cs
using IteratorPattern;

List<Book> books = BookReader.LoadBooksFromJson("book.json");
Console.WriteLine("Demo App: Iterator Pattern");

Console.WriteLine("--- Unsorted: ---");
foreach (Book book in books)
{
    Console.WriteLine(book.Title);
}

books.Sort(); // This causes the error

Console.WriteLine("\n--- Sorted: ---");
foreach (Book book in books)
{
    Console.WriteLine(book.Title);
}
```

**Book.cs**:
```cs
namespace IteratorPattern;

public class Book
{
    public string Title { get; set; }
    public string Author { get; set; }
    public string Genre { get; set; }
    public int Year_Published { get; set; }
    public string ISBN { get; set; }
}
```

**BookReader.cs**:
```cs
namespace IteratorPattern;

using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

public class BookReader
{
    public static List<Book> LoadBooksFromJson(string filePath)
    {
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException("The specified file was not found.", filePath);
        }

        var jsonData = File.ReadAllText(filePath);
        return JsonConvert.DeserializeObject<List<Book>>(jsonData);
    }
}
```

Attempting to run this will throw an error:
```
Unhandled exception. System.InvalidOperationException: Failed to compare two elements in the array.
 ---> System.ArgumentException: At least one object must implement IComparable.
```

