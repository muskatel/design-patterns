namespace FactoryPatternsInClass;

public class Tea:Beverage
{
    // add tea leaves
    // brew the tea
    // serve
    /*
    public static Tea MakeTea()
    {
        Tea t = new Tea();
        t.Prepare();
        return t;
    }
    */

    private Tea()
    {
        Console.WriteLine("Getting tea cup."); //memory init
    }

    public void MakeTeaWithLeaves(int gramsOfLooseLeaves, bool addHoney)
    {
        Console.WriteLine("Warming machine...");
        Console.WriteLine($"Measuring out {gramsOfLooseLeaves}g of leaves");

        if (addHoney)
            Console.WriteLine($"Adding honey into empty cup");
    }
    
    public void MakeTeaWithBags(int teaBags, bool addHoney)
    {
        Console.WriteLine("Warming machine...");
        Console.WriteLine($"Colleting {teaBags} teabags");

        if (addHoney)
            Console.WriteLine($"Adding honey into empty cup");
    }
    
    protected override void Prepare()
    {
        Console.WriteLine("Adding leaves.");
        Console.WriteLine("Brewing tea.");
        Console.WriteLine("Serving...");
    }

    public override void Consume()
    {
        Console.WriteLine("This is tea!");
    }
}