namespace FactoryPatternsInClass;

public class Coffee: Beverage
{
    /// <summary>
    /// Factory Method
    /// </summary>
    /// <returns></returns>
    public static Coffee MakeCoffee()
    {
        Coffee c = new Coffee();
        c.Prepare();
        return c;
    }

    private Coffee()
    {
        Console.WriteLine("Warming machine ...");
    }

    protected override void Prepare()
    {
        Console.WriteLine("Grinding beans.");
        Console.WriteLine("Adding grinds to filter.");
        Console.WriteLine("Brewing coffee.");
        Console.WriteLine("Serving...");
    }

    public override void Consume()
    {
        Console.WriteLine("This is coffee!");
    }
}