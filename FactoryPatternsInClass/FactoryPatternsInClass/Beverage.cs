namespace FactoryPatternsInClass;

public abstract class Beverage
{
    protected abstract void Prepare();
    public abstract void Consume();
}