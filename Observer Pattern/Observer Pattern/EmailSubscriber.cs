﻿namespace Observer_Pattern;

public class EmailSubscriber:IObserver
{
    private string email;

    public EmailSubscriber(string email)
    {
        this.email = email;
    }
    public void Update(IObservable subject)
    {
        YouTubeChannel? notificationService = subject as YouTubeChannel;
        if (notificationService != null)
        {
            Console.WriteLine($"An email was sent to {email} about {notificationService.State}");
        }
    }
}