﻿// See https://aka.ms/new-console-template for more information

using Observer_Pattern;

// I create 10 subscribers
Subscriber John = new Subscriber("John");
Subscriber Sarah = new Subscriber("Sarah");
Subscriber Michael = new Subscriber("Michael");
Subscriber Jessica = new Subscriber("Jessica");
Subscriber Robert = new Subscriber("Robert");
Subscriber Emily = new Subscriber("Emily");
Subscriber David = new Subscriber("David");
Subscriber Jennifer = new Subscriber("Jennifer");
EmailSubscriber William = new EmailSubscriber("William@mail.com");
EmailSubscriber Amanda = new EmailSubscriber("Amanda@mail.com");

// Create a new service that can be subscribed to

YouTubeChannel CraigsAmazingYoutubeChannel = new YouTubeChannel();

// Add subscribers
// some are regular subscribers
CraigsAmazingYoutubeChannel.Subscribe(John);
CraigsAmazingYoutubeChannel.Subscribe(Sarah);
CraigsAmazingYoutubeChannel.Subscribe(Michael);
CraigsAmazingYoutubeChannel.Subscribe(Jessica);
CraigsAmazingYoutubeChannel.Subscribe(Robert);
CraigsAmazingYoutubeChannel.Subscribe(Emily);
CraigsAmazingYoutubeChannel.Subscribe(David);
CraigsAmazingYoutubeChannel.Subscribe(Jennifer);
// these last two are e-mail subscribers
CraigsAmazingYoutubeChannel.Subscribe(William);
CraigsAmazingYoutubeChannel.Subscribe(Amanda);

// Set the state of our service and then notify subscribers
CraigsAmazingYoutubeChannel.State = "Latest Video: Clickbait Title!";
CraigsAmazingYoutubeChannel.Notify();

// Some subscribers unsubscribe :(
CraigsAmazingYoutubeChannel.Unsubscribe(Michael);
CraigsAmazingYoutubeChannel.Unsubscribe(Emily);
CraigsAmazingYoutubeChannel.Unsubscribe(David);
CraigsAmazingYoutubeChannel.Unsubscribe(Jennifer);
CraigsAmazingYoutubeChannel.Unsubscribe(Amanda);

PagerSubscriber oldGuy = new PagerSubscriber();
CraigsAmazingYoutubeChannel.Subscribe(oldGuy);

// a Desperate attempt to keep subscribers
// State is updated and the remaining subscribers are notified
CraigsAmazingYoutubeChannel.State = "Latest Video: I'm,sorry";
CraigsAmazingYoutubeChannel.Notify(); 