﻿namespace Observer_Pattern;

public interface IObserver
{
    void Update(IObservable subject);
}