﻿namespace Observer_Pattern;

public interface IObservable
{
    void Subscribe(IObserver observer);

    void Unsubscribe(IObserver observer);

    void Notify();
}