﻿namespace Observer_Pattern;

public class YouTubeChannel:IObservable
{
    private List<IObserver> _subscribers = new List<IObserver>();

    public string State;
    
    public void Subscribe(IObserver observer)
    {
        _subscribers.Add(observer);
    }

    public void Unsubscribe(IObserver observer)
    {
        if (_subscribers.Contains(observer))
        {
            _subscribers.Remove(observer);
        }
    }

    public void Notify()
    {
        foreach (IObserver observer in _subscribers)
        {
            observer.Update(this);
        }
    }
}