namespace Observer_Pattern;

public class PagerSubscriber:IObserver
{
    public void Update(IObservable subject)
    {
        YouTubeChannel? notificationService = subject as YouTubeChannel;
        if (notificationService != null)
        {
            Console.WriteLine($"Pager subscriber notified about {notificationService.State}");
        }

    }
}