﻿namespace Observer_Pattern;

public class Subscriber:IObserver
{
    private string name;
    public Subscriber(string name)
    {
        this.name = name;
    }
    public void Update(IObservable subject)
    {
        YouTubeChannel? notificationService = subject as YouTubeChannel;
        if (notificationService != null)
        {
            Console.WriteLine($"{name} was notified about {notificationService.State}");
        }
    }

}