namespace AdapterPattern;

public interface INewPrinter
{
    void PrintText(string text);
    void PrintGraphics(string graphics);
}