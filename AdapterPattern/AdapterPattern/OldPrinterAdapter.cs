namespace AdapterPattern;

public class OldPrinterAdapter:INewPrinter
{
    private OldPrinter _oldPrinter;

    public OldPrinterAdapter(OldPrinter oldPrinter)
    {
        _oldPrinter = oldPrinter;
    }
    public void PrintText(string text)
    {
        _oldPrinter.LegacyPrintFunction(text);
    }

    public void PrintGraphics(string graphics)
    {
        Console.WriteLine("Old Printer cannot print Graphics");
    }
}