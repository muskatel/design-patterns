namespace AdapterPattern;

public class FancyModernPrinter:INewPrinter
{
    public void PrintText(string text)
    {
        Console.WriteLine(text);
    }

    public void PrintGraphics(string graphics)
    {
        Console.WriteLine(graphics);
    }
}