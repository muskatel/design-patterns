namespace AdapterPattern;

public class OldPrinter
{
    public void LegacyPrintFunction(string text)
    {
        Console.WriteLine($"Old Printer: {text}");
    }
}