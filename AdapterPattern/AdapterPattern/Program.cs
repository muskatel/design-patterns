﻿// See https://aka.ms/new-console-template for more information

using AdapterPattern;


//oldFaithful.PrintText("Hello, Class!");

INewPrinter adaptedPrinter = new OldPrinterAdapter(new OldPrinter());
adaptedPrinter.PrintText("Hello, Class!");
adaptedPrinter.PrintGraphics("😘");


INewPrinter newPrinter = new FancyModernPrinter();
newPrinter.PrintText("Hello, Class!");
newPrinter.PrintGraphics("😘");