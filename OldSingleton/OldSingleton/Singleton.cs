namespace OldSingleton;

public class Singleton
{
    public int Data = 42;
    
    private static Singleton? _instance = null;

    private Singleton()
    {
        // this is private
    }

    public static Singleton GetInstance()
    {
        if (_instance == null)
        {
            _instance = new Singleton();
        }

        return _instance;
    }
    
    
    
}