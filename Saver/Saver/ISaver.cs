namespace Saver;

public interface ISaver
{
    public void Save();
}