using Newtonsoft.Json;

namespace Saver;

public class JsonSaver:ISaver
{
    private Pet _pet;

    public JsonSaver(Pet pet)
    {
        this._pet = pet;
    }


    public void Save()
    {
        String petString = JsonConvert.SerializeObject(_pet);
        
        StreamWriter output = new StreamWriter($"pet_{_pet.name}.json");
        output.WriteLine(petString);
        output.Close();
    }
}