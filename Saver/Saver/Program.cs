﻿// See https://aka.ms/new-console-template for more information

using Saver;

Console.WriteLine("Hello, World!");

Pet pet1 = new Pet();
pet1.name = "Steve";
pet1.species = "cat";


StringSaver ss = new StringSaver(pet1);
ss.Save();

JsonSaver js = new JsonSaver(pet1);
js.Save();
