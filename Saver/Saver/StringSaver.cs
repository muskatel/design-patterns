namespace Saver;

//adapter
//file writer to our interface
public class StringSaver:ISaver
{
    private Pet _pet;

    public StringSaver(Pet pet)
    {
        _pet = pet;
    }
    public void Save()
    {
        string saveString = _pet.name + " " + _pet.species;
        StreamWriter output = new StreamWriter($"pet_{_pet.name}.txt");
        output.WriteLine(saveString);
        output.Close();
    }
}