namespace Peoples;

public class Professor : Person
{
    public Professor(string fullName):base(fullName)
    {
        title = "Prof.";
    }

    public new string getName()
    {
        return "You're not a professor!";
    }
}