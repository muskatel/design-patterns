namespace Peoples;

public class Person
{
    private string fullName;
    private string dateOfBirth;
    protected string title; //Assume male

    public Person()
    {
        fullName = "Undefined";
        title = "Mr";
    }
    public Person(string fullName)
    {
        this.fullName = fullName;
        title = "Mr";
    }

    public string getName()
    {
        return $"{title} {fullName}";
    }
    
}