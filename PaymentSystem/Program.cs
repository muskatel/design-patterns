﻿// See https://aka.ms/new-console-template for more information

using PaymentSystem;

float amount = 0;

Console.WriteLine("Hello, Welcome to MEGA-Bank!");


// Create user with new debit account

User craig = new User("Craig Marais", 420.0f);
Console.WriteLine(craig);

craig.DebitAccountNumber = Bank.Instance.CreateDebitAccount(craig.Name, "hello123");


// Deposit 400 to debit account

amount = 400;
if (amount < craig.CashInHand)
{
    if (Bank.Instance.AccessAccount(craig.DebitAccountNumber,"hello123").DepositCash(amount))
    {
        craig.CashInHand -= amount;
    }
}

// Check Status

Console.WriteLine(craig);

Bank.Instance.AccessAccount(craig.DebitAccountNumber,"hello123").ShowBalance();

// Withdraw 200 from debit account

amount = 200;
if (Bank.Instance.AccessAccount(craig.DebitAccountNumber,"hello123").WithdrawCash(amount))
{
    craig.CashInHand += amount;
}

// create new credit accoutn for same user

craig.CreditAccountNumber = Bank.Instance.CreateCreditAccount(craig.Name, "hello123");
Bank.Instance.AccessAccount(craig.CreditAccountNumber,"hello123").ShowBalance();
amount = 800;
if (Bank.Instance.AccessAccount(craig.CreditAccountNumber,"hello123").WithdrawCash(amount))
{
    craig.CashInHand += amount;
}

// Buy something

Payment lunch = new CreditCardPayment(craig, 53f, craig.DebitAccountNumber, "Spar");
//lunch.HandlePayment();

// Check Status

Console.WriteLine(craig);

Console.WriteLine("\n\n-----------------------------------------\n");

Payment choclate = new CashPayment(craig, 23.99f);
//choclate.HandlePayment();

Console.WriteLine("\n\n-----------------------------------------\n");

Payment computer_screen = new CreditCardPayment(craig, 780f, craig.CreditAccountNumber, "Komplett");

Bank.Instance.AccessAccount(craig.CreditAccountNumber,"hello123").DepositCash(1000);

 computer_screen.HandlePayment();


Bank.Instance.AccessAccount(craig.DebitAccountNumber,"hello123").ShowBalance();

Bank.Instance.AccessAccount(craig.DebitAccountNumber, "hello123").PrintTransactions();

Bank.Instance.AccessAccount(craig.CreditAccountNumber,"hello123").ShowBalance();

Bank.Instance.AccessAccount(craig.CreditAccountNumber, "hello123").PrintTransactions();

