﻿namespace PaymentSystem;

public abstract class CardPayment:Payment
{
    private Account _account;
    protected string _password;
    protected string _balanceErrorMessage = "Balance Error";
    protected string _merchant;
    
    
    public CardPayment(User payee, float amount, string? sourceAccount = null, string merchant = "Unknown Merchant") : base(payee, amount, sourceAccount)
    {
        _merchant = merchant;
        if (sourceAccount == null)
        {
            Console.WriteLine("Error: Missing Account Number");
        }
    }

    protected override bool PasswordCheck()
    {
        Read:
        Console.Write("\nPlease enter account password (x/q to quit):");
        string passwordEntered = Console.ReadLine();

        if (passwordEntered == "q" || passwordEntered == "x")
        {
            Console.WriteLine("Quit without entering password");
            return false;
        }
        
        if (string.IsNullOrEmpty(passwordEntered))
            goto Read;
        
        

        _account = Bank.Instance.AccessAccount(_sourceAccount, passwordEntered);
        
        if (_account == null)
        {
            return false;
        }

        _password = passwordEntered;
        return true;
    }

    protected override bool AccountBalanceCheck()
    {
        if(!_account.CheckBalanceAgainstAmount(_amount))
        {
            Console.WriteLine($"{_balanceErrorMessage}");
            return false;
        }

        return true;
    }

    protected override bool ProcessPayment()
    {
        return _account.Purchase(_amount, _password, _merchant);
    }
}