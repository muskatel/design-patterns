﻿namespace PaymentSystem;

public enum PaymentStatus
{
    Wait,
    Fail,
    Success
}

public abstract class Payment
{
    protected User _payee;
    protected float _amount;
    protected string _sourceAccount;
    
    public Payment(User payee, float amount, string? sourceAccount = null)
    {
        _payee = payee;
        _amount = amount;
        _sourceAccount = sourceAccount;
    }
    
    public bool HandlePayment()
    {
        /*
        if (PasswordCheck())
        {
            if (AccountBalanceCheck())
            {
                ProcessPayment();
        
                Console.WriteLine("Approved: Purchase is complete");
                return true;
            }
            else
            {
                Console.WriteLine("Declined: Account Balance Error");
                return false;
            }
        }
        else
        {
            Console.WriteLine("Declined: Password Error");
            return false;
        }
        */
        
        if (!PasswordCheck())
        {
            Console.WriteLine("Declined: Password Error");
            return false;
        }

        if (!AccountBalanceCheck())
        {
            Console.WriteLine("Declined: Account Balance Error");
            return false;
        }

        ProcessPayment();
        
        Console.WriteLine("Approved: Purchase is complete");
        return true;
        
    }

    protected abstract bool PasswordCheck();
    protected abstract bool AccountBalanceCheck();
    
    protected abstract bool ProcessPayment();
}