﻿namespace PaymentSystem;

public class DebitCardPayment:CardPayment
{
    public DebitCardPayment(User payee, float amount, string? sourceAccount = null, string merchant = "Unknown Merchant") : base(payee, amount, sourceAccount, merchant)
    {
        _balanceErrorMessage = "Insufficient balance in account";
    }
}