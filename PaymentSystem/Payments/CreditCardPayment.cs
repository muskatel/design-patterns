﻿namespace PaymentSystem;

public class CreditCardPayment:CardPayment
{
    public CreditCardPayment(User payee, float amount, string? sourceAccount = null, string merchant = "Unknown Merchant") : base(payee, amount, sourceAccount, merchant)
    {
        _balanceErrorMessage = "Insufficient credit available";
    }
}