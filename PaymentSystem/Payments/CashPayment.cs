﻿namespace PaymentSystem;

public class CashPayment:Payment
{
    private float cashHandedOver = 0.0f;
    public bool HandOverCash()
    {
        Read:
        cashHandedOver = 0.0f;
        Console.Write("\nPlease enter cash amount to hand over (x/q to quit):");
        string input = Console.ReadLine();


        if (input == "q" || input == "x")
        {
            return false;
        }
        
        if (string.IsNullOrEmpty(input))
            goto Read;
        
        if (!float.TryParse(input, out cashHandedOver))
            goto Read;


        if (_payee.CashInHand < cashHandedOver)
        {
            Console.WriteLine("You dont have that much.");
            goto Read;
        }
        
        if (cashHandedOver <= 0.0)
        {
            Console.WriteLine("Cannot hand over negative or zero money.");
            goto Read;
        }

        if (cashHandedOver < _amount)
        {
            Console.WriteLine("That is not enough money.");
            goto Read;
        }
        
        return true;
    }
    
    public CashPayment(User payee, float amount):base(payee, amount, null)
    {
        Console.WriteLine($"Created a new cash payment for amount of {_amount}");
    }

    protected override bool PasswordCheck()
    {
        return true;
    }

    protected override bool AccountBalanceCheck()
    {
        if (!HandOverCash())
        {
            Console.WriteLine($"Could not get money from {_payee.Name}.");
            return false;
        }
        
        return true;
    }

    protected override bool ProcessPayment()
    {
        float change = cashHandedOver - _amount;
        Console.WriteLine($"The change for the purchase is {change}");
        _payee.CashInHand -= _amount;
        return true;
    }
}