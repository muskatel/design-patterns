﻿namespace PaymentSystem;

public class User
{
    private string _name;
    private float _cashInHand;
    private string _debitAccountNumber;
    private string _creditAccountNumber;

    public User(string name, float cashInHand)
    {
        Name = name;
        CashInHand = cashInHand;
    }
    
    public string DebitAccountNumber
    {
        get => _debitAccountNumber;
        set => _debitAccountNumber = value;
    }

    public string CreditAccountNumber
    {
        get => _creditAccountNumber;
        set => _creditAccountNumber = value;
    }

    public string Name
    {
        get => _name;
        set => _name = value;
    }

    public float CashInHand
    {
        get => _cashInHand;
        set => _cashInHand = value;
    }

    public override string ToString()
    {
        return $"\n{_name}" +
               $"\n Cash in hand: {_cashInHand}" +
               $"\n Debit account number:  {_debitAccountNumber}" +
               $"\n Credit account number: {_creditAccountNumber}";
    }
}