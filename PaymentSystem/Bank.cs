﻿namespace PaymentSystem;

public class Bank
{
    private static Bank _instance;
    
    private List<Account> _accounts;
    private string _bankName;

    protected Bank(string bankName)
    {
        _accounts = new List<Account>();
        _bankName = bankName;
    }

    public static Bank Instance
    {
        get
        {
            if (_instance == null)
                _instance = new Bank("MEGA-Bank");
            return _instance;
        }
    }

    public string CreateDebitAccount(string accountHolderName, string password)
    {
        Account newDebitAccount = new DebitAccount(accountHolderName, NextAccountNumber('D'), password, _bankName);
        _accounts.Add(newDebitAccount);
        Console.WriteLine($"\n{_bankName}:" +
                          $"\n Created new debit account: {newDebitAccount}" +
                          $"\n Owner is {accountHolderName}");
        return newDebitAccount.ToString();
    }
    
    public string CreateCreditAccount(string accountHolderName, string password)
    {
        Account newCreditAccount = new CreditAccount(accountHolderName, NextAccountNumber('C'), password, _bankName);
        _accounts.Add(newCreditAccount);
        Console.WriteLine($"\n{_bankName}:" +
                          $"\n Created new credit account: {newCreditAccount}" +
                          $"\n Owner is {accountHolderName}");
        return newCreditAccount.ToString();
    }
    
    /*
    // Without creating another layer of complexity, I cannot ensure that the deposit is removed from the users cash balance
    public string CreateDebitAccount(string accountHolderName, string password, float initialDeposit)
    {
        String newDebitAccountNumber = CreateDebitAccount(accountHolderName, password);
        GetAccount(newDebitAccountNumber).DepositCash(initialDeposit);
        return newDebitAccountNumber;
    }
    */
    
    private Account? AccessAccount(string accountNumber)
    {
        foreach (Account acc in _accounts)
        {
            if (acc.ToString() == accountNumber)
            {
                return acc;
            }
        }

        return null;
    }

    public Account? AccessAccount(string accountNumber, string password)
    {
        Account? acc = AccessAccount(accountNumber);
        if(acc != null && acc.Password == password)
        {
            return acc;
        }

        return null;
    }

    private string NextAccountNumber(char typePrefix = 'U')
    {
        Random r = new Random();
        NewAccount:
        string num = (r.Next(1, 1000000)).ToString();
        string accountNumber = num.PadLeft(11,'0');
        accountNumber = accountNumber.ToString().PadLeft(12,typePrefix);
        if (AccessAccount(accountNumber) != null)
            goto NewAccount;    // At this point we have found a duplicate and need to restart
        
        return accountNumber;
    }
    
    
}