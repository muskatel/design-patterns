﻿namespace PaymentSystem;

public class Transaction
{
    private string _type;
    private string _amount;
    private string _note;
    private string _balance;

    public Transaction(string type, string amount, string balance, string note)
    {
        _type = type;
        _amount = amount;
        _balance = balance;
        _note = note;
    }

    public override string ToString()
    {
        return $"Type:{_type} - Amount:{_amount} - Balance:{_balance} - Note:{_note}";
    }
}