﻿namespace PaymentSystem;

public class DebitAccount : Account
{
    public DebitAccount(string accountHolderName, string accountNumber, string password, string bankName)
    {
        _bankName = bankName;
        _accountHolderName = accountHolderName;
        _accountNumber = accountNumber;
        _password = password;
        _balance = 0.0f;

        _balanceString = "Balance";

        _withdrawError = "Cannot withdraw more than account balance";
        
        _purchaseError = "Debit Purchase Error";

        _transactions = new List<Transaction>();
    }

}