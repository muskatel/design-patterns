﻿namespace PaymentSystem;

public class CreditAccount:Account
{
    private float _maxCredit = 1000f;
    public CreditAccount(string accountHolderName, string accountNumber, string password, string bankName)
    {
        _bankName = bankName;
        _accountHolderName = accountHolderName;
        _accountNumber = accountNumber;
        _password = password;
        _balance = _maxCredit;

        _balanceString = "Remaining credit is";
        
        _withdrawError = "Cannot withdraw more than remaining credit";
        _purchaseError = "Credit Purchase Error";
        
        _transactions = new List<Transaction>();
    }

    public bool PayOffOwedAmount(float amount, DebitAccount sourceAccount)
    {
        // this could be a payment from the debit account
        throw new NotImplementedException();
    }

    public bool AdjustCredit(float newCreditAmount)
    {
        // TODO: test that this is functionally correct
        float difference = newCreditAmount - _maxCredit;
        
        float owed = _balance - _maxCredit;

        if (difference < owed)
            return false;

        _maxCredit += difference;
        _balance += difference;

        return true;
    }
}