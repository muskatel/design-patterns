﻿namespace PaymentSystem;

public abstract class Account:IComparable<Account>
{
    protected string _bankName;
    protected string _accountHolderName;
    protected string _accountNumber;
    protected string _password;
    protected float _balance;

    protected List<Transaction> _transactions;
    
    protected string _balanceString = "";

    protected string _withdrawError = "";
    protected string _purchaseError = "";

    public string Password => _password;

    public void ShowBalance()
    {
        Console.WriteLine($"\n{_bankName}:" +
                  $"\n Account:{_accountNumber}" +
                  $"\n {_balanceString}: {BalanceToFixedLengthString()}");
    }

    public bool CheckBalanceAgainstAmount(float amount)
    {
        return _balance >= amount;
    }
    public bool DepositCash(float amount)
    {
        if (amount <= 0.0f)
        {
            Console.WriteLine($"\n{_bankName}:" +
                              $"\n Deposit failed: {amount} " +
                              $"\n Cannot deposit a negative number");
            return false;
        }

        _balance += amount;
        _transactions.Add(new Transaction("Deposit ", amount.ToString(),BalanceToFixedLengthString(), "Banking"));
        Console.WriteLine($"\n{_bankName}:" +
                          $"\n Deposited {amount}\n into {_accountNumber}" +
                          $"\n {_balanceString}: {BalanceToFixedLengthString()}");
        return true;
    }
        
    public bool WithdrawCash(float amount)
    {
        if (amount <= 0.0f)
        {
            Console.WriteLine($"\n{_bankName}:" +
                              $"\n Withdraw failed: {amount} " +
                              $"\n Cannot withdraw a negative number");
            return false;
        }
        
        if (amount > _balance)
        {
            Console.WriteLine($"\n{_bankName}:" +
                              $"\n Withdraw failed: {amount} " +
                              $"\n {_withdrawError}");
            return false;
        }

        _balance -= amount;
        _transactions.Add(new Transaction("Withdraw", amount.ToString(),BalanceToFixedLengthString(), "Banking"));
        Console.WriteLine($"\n{_bankName}:" +
                          $"\n Withdrew {amount} from {_accountNumber}" +
                          $"\n {_balanceString}: {BalanceToFixedLengthString()}");
        return true;
    }

    public bool Purchase(float amount, string password, string merchant)
    {
        if (password != _password)
        {
            Console.WriteLine($"\n{_bankName}:" +
                              $"\n Purchase from {merchant} failed: {amount}" +
                              $"\n Invalid password");
            return false;
        }

        if (amount <= 0.0f)
        {
            Console.WriteLine($"\n{_bankName}:" +
                              $"\n Purchase from {merchant} failed: {amount}" +
                              $"\n Cannot spend a negative number");
            return false;
        }
        
        if (amount > _balance)
        {
            Console.WriteLine($"\n{_bankName}:" +
                              $"\n Purchase from {merchant} failed: {amount}" +
                              $"\n {_purchaseError}");
            return false;
        }

        _balance -= amount;
        _transactions.Add(new Transaction("Purchase ", amount.ToString(),BalanceToFixedLengthString(), merchant));
        Console.WriteLine($"\n{_bankName}:" +
                          $"\n Purchase {amount} using {_accountNumber}" +
                          $"\n {_balanceString}: {BalanceToFixedLengthString()}");
        return true;
    }

    public int CompareTo(Account acc)
    {
        return String.Compare(_accountNumber,acc._accountNumber);
    }

    public override bool Equals(object? obj)
    {
        return _accountNumber == ((Account)obj)._accountNumber;
    }

    public override string ToString()
    {
        return _accountNumber;
    }

    public void PrintTransactions()
    {
        Console.WriteLine($"\n{_bankName}");
        Console.WriteLine($" {_transactions.Count} Transactions for {_accountNumber}");
        foreach (Transaction transaction in _transactions)
        {
            Console.WriteLine($" {transaction}");
        }
    }

    protected string BalanceToFixedLengthString()
    {
        return $"{_balance:F2}";
    }
}